## 03.06.2018
## Risto Veijonen
## Angular 5 Demo application for Klinik Healthcare Solutions

### To get going just run
`yarn add && ng serve`
### And open into browser:
<http://localhost:4200/>

## Requirements not met for
**Fetching data from: <https://vision.klinik.fi/elukat.json> due to CORS error.**
- fixes
1. Creating a server-side script to pull the data with nodejs, php or similar.
2. Enabling CORS-requests on designated server.

**Sorting of lists seperately**
- fixes
1. Emiting data between the templates.
2. Using a parent template to handle states.
3. Using a service to handle states.