import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({ 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})

export class AnimalService {
  private url = "assets/animals.json";
  // private url = 'https://vision.klinik.fi/elukat.json';
  constructor(private http: HttpClient) {  }

  getAnimals(): Observable<any> {
    return this.http.get(this.url, httpOptions);
  }
}
