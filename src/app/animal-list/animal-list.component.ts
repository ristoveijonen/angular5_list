import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { AnimalService } from "../animal.service";

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.css']
})

export class AnimalListComponent implements OnInit {
  displayedColumns = ['name', 'breed', 'age', 'location', 'actions'];
  animalData = new MatTableDataSource();
  favoritesData = new MatTableDataSource();
  catAverageAge: number = 0;
  catUniqueCities: number = 0;
  empty = {name: 'This list is currently empty!', breed: '', age: null, location: ''};
  private listNow: string = "";
  private species = [];
  constructor(private svc: AnimalService) { }

  @ViewChild(MatSort) sort: MatSort;

  checkIfEmpty() {
    if(species_data.length === 0) {
      species_data.push(this.empty);
    } else if(species_data.length > 0 && species_data.indexOf(this.empty) !== -1 ){
      species_data.splice(species_data.indexOf(this.empty), 1);
    }
    if(favorites_data.length === 0) {
      favorites_data.push(this.empty);
    } else if(favorites_data.length > 0 && favorites_data.indexOf(this.empty) !== -1 ){
      favorites_data.splice(favorites_data.indexOf(this.empty), 1);
    }
  }

  calculateCatAge() {
    let catAges: number = 0;
    let catCities = [];
    this.svc.getAnimals().subscribe(data => {
      for(const INDEX in data['cats']) {
        const CAT = data['cats'][INDEX];
        catAges += CAT.age;
        if (catCities.indexOf(CAT.location) === -1) {
          catCities.push(CAT.location);
        }
      };
      this.catAverageAge = catAges / data['cats'].length;
      this.catUniqueCities = catCities.length;
    });
  }

  addToFavorites(animal) {
    species_data.splice(species_data.indexOf(animal), 1);
    this.checkIfEmpty();
    this.animalData = new MatTableDataSource(species_data);
    this.animalData.sort = this.sort;
    if(animal !== this.empty){
      favorites_data.push(animal);
      this.favoritesData = new MatTableDataSource(favorites_data);
      this.favoritesData.sort = this.sort;
    }
  }

  removeFromFavorites(animal) {
    favorites_data.splice(favorites_data.indexOf(animal), 1);
    this.checkIfEmpty();
    this.favoritesData = new MatTableDataSource(favorites_data);
    this.favoritesData.sort = this.sort;
    if(this.listNow === animal.species) {
      species_data.push(animal);
      this.animalData = new MatTableDataSource(species_data);
      this.animalData.sort = this.sort;
    }
  }

  ngOnInit() {
    this.svc.getAnimals().subscribe(data => {
      this.species = Object.keys(data);
      this.CreateList(this.species[0]);
      this.calculateCatAge();
      favorites_data.push(this.empty);
      this.favoritesData = new MatTableDataSource(favorites_data);
      this.favoritesData.sort = this.sort;
    });
  }

  CreateList(SPECIES: string) {
    this.listNow = SPECIES;
    this.svc.getAnimals().subscribe(data => {
      this.BuildDataSet(data, SPECIES)
      .then(() => {
        this.checkIfEmpty();
        this.animalData = new MatTableDataSource(species_data)
        this.animalData.sort = this.sort
      });
    });
  }

  BuildDataSet(data, SPECIES: string = "cats") {
    species_data = [];
    const PROMISE = new Promise((resolve, reject) => {
      let total: number = 0;
      for(const INDEX in data[SPECIES]) {
        const ANIMAL = data[SPECIES][INDEX];
        let add = false;
        total++;
        ANIMAL.species = SPECIES;
        species_data.push(ANIMAL);
        for (const FAVORITE in favorites_data) {
          if(JSON.stringify(favorites_data[FAVORITE]) === JSON.stringify(ANIMAL)) {
            species_data.splice(species_data.indexOf(ANIMAL), 1);
            total--;
          }
        }
        if(species_data.length === total) {
          resolve();
        }
      };
    });
    return PROMISE;
  }
}

export interface AnimalSpecies {
  name: string;
  breed: string;
  age: number;
  location: string;
}

let species_data: AnimalSpecies[] = [];
let favorites_data: AnimalSpecies[] = [];